<%--
  Created by IntelliJ IDEA.
  User: akohsin
  Date: 18.04.2020
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Formularz</title>
</head>
<body>

<%--action -> zawiera adres servletu lub pliku jsp do którego wysyłamy dane z formularza.--%>
<%--method -> zmienia sposób przesyłu danych --%>
<form action="welcome.jsp" method="get">
    Imie: <input type="text" name="imie">
    <br/>
    Nazwisko: <input type="text" name="nazwisko">
    <br/>
    <input type="submit"> <%--guzik zatwierdzający przesłanie danych z formularza--%>
</form>

</body>
</html>
