<%--
  Created by IntelliJ IDEA.
  User: akohsin
  Date: 19.04.2020
  Time: 10:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka mnożenia</title>
</head>
<body>
<a href="formularz.jsp">Formularz</a>
<a href="formularz_kolumn.jsp">Formularz kolumn</a>
<a href="formularz_tabliczka_mnozenia.jsp">Formularz tabliczka mnożenia</a>

<%
    Integer iloscKolumn = null;
    Integer iloscWierszy = null;
    // fragment Java odpowiadający za odczytanie danych
    if (request.getParameter("wiersze") == null || request.getParameter("wiersze").isEmpty()) {
        out.print("Brak ilosci wierszy");
    } else {
        iloscWierszy = Integer.parseInt(request.getParameter("wiersze"));
    }
    if (request.getParameter("kolumny") == null || request.getParameter("kolumny").isEmpty()) {
        out.print("Brak ilosci kolumn");
    } else {
        iloscKolumn = Integer.parseInt(request.getParameter("kolumny"));
    }
%>
<%
    if (iloscKolumn != null && iloscKolumn != null) {
%>
<table>
    <%
        for (int i = 0; i < iloscWierszy; i++) {
            out.print("<tr>");
            for (int j = 0; j < iloscKolumn; j++) {
                out.print("<td style=\"border: 1px solid;\">");
                out.print((i + 1) * (j + 1));
                out.print("</td>");
            }
            out.print("</tr>");
        }
    %>
</table>
<%
    }
%>
</body>
</html>
