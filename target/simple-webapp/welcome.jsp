<%--
  Created by IntelliJ IDEA.
  User: akohsin
  Date: 18.04.2020
  Time: 13:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
</head>
<body>
<a href="formularz.jsp">Formularz</a>
<a href="formularz_kolumn.jsp">Formularz kolumn</a>
<a href="formularz_tabliczka_mnozenia.jsp">Formularz tabliczka mnożenia</a>

<%--    wyjście (out) na treść html --%>
<h2>
    <%
        String imie = request.getParameter("imie");
        String nazwisko = request.getParameter("nazwisko");
        if (imie != null && !imie.isEmpty() && nazwisko != null && !nazwisko.isEmpty()) {
            out.print("Witoj " + imie + " " + nazwisko + "!");
        } else {
            out.print("Przedstaw się!");
        }
    %>
</h2>

</body>
</html>
