<%--
  Created by IntelliJ IDEA.
  User: akohsin
  Date: 19.04.2020
  Time: 10:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Kolumny</title>
</head>
<body>
<a href="formularz.jsp">Formularz</a>
<a href="formularz_kolumn.jsp">Formularz kolumn</a>
<a href="formularz_tabliczka_mnozenia.jsp">Formularz tabliczka mnożenia</a>

<table style="border: 1px solid;">
    <tr>
        <%
            if (request.getParameter("ilosc") == null) {
                out.print("<td>Brak ilosci</td>");
            }
            int ilosc = Integer.parseInt(request.getParameter("ilosc"));
            for (int i = 0; i < ilosc; i++) {
                out.print("<td>");
                out.print(i + 1);
                out.print("</td>");
            }
        %>
    </tr>
</table>
</body>
</html>
